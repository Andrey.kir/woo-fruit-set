<?php
global $savedFruits;
?>
<div class="set_container">
    <div class="col-6 pr-15">
        <?
        if (get_the_post_thumbnail_url($post, 'full')) {
            ?>
            <img class="set_img"
                 data-thumb-id ="<? echo get_post_thumbnail_id($post->ID)?>"
                 src="<?php echo get_the_post_thumbnail_url($post, 'full') ?>"
                 alt="">
            <?
        } ?>
            <div class="set_descr">
                <?
                echo $post->post_excerpt;
                ?>
            </div>

    </div>
    <div class="col-6 pl-15 ">
        <h1 clwoocommerce-breadcrumb
            breadcrumbsass="fruit-product_title product-title product_title entry-title">
            <span class="fruit-product_title"><? echo get_the_title() ?></span>
        </h1>
        <!-- include set_composition list -->
        <?php
        include(FRUIT_SET_DIR . 'templates/front/template-parts/lists/set_composition.php');
        ?>
    </div>
</div>
<!-- include set_list list -->
<div class="set_list">
    <?php
    //        include(FRUIT_SET_DIR . 'templates/front/template-parts/lists/set_list.php');
    include(FRUIT_SET_DIR . 'templates/front/template-parts/lists/new_set_list.php');
    ?>

</div>


