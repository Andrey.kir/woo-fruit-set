<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit();
}
use Carbon_Fields\Container;
use Carbon_Fields\Field;


if ( ! class_exists( 'FRUIT_Set_Carbon' ) ) {
    class FRUIT_Set_Carbon
    {

        private static $instance;

        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct() {
//            add_action( 'init', [ $this, 'carbon_get_woo_cats' ]);
            add_action( 'after_setup_theme', [ $this, 'crb_load' ] );
            add_action( 'carbon_fields_register_fields', [ $this, 'crb_attach_theme_options' ] );
        }

        public function crb_load() {

            require_once( FRUIT_SET_DIR . 'vendor/autoload.php' );
            \Carbon_Fields\Carbon_Fields::boot();
        }

        public function crb_attach_theme_options() {

//            $productCategories = $this->carbon_get_woo_cats();

            Container::make( 'post_meta', __( 'Set Data' ) )
                ->where( 'post_type', '=', 'set' )
                ->add_fields( array(
                    Field::make( "multiselect", "crb_available_cats", "Category" )
                        ->add_options( array( $this, 'get_product_cats' ) ),
                ) );
        }

        public function carbon_get_woo_cats() {
//            $args = array(
//                'orderby'  => 'title',
//                'order'    => 'ASC',
//                'taxonomy' => 'product_cat',
//            );
            $categories = get_categories();
            var_dump($categories);die();
        }
    }

    FRUIT_Set_Carbon::get_instance();
}