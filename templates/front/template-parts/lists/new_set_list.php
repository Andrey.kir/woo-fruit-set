<?php
global $savedFruits;
?>

<div class="set_list">
    <h2 class="set_list__ttl">Состав набора</h2>
    <?php foreach ($savedFruits as $key => $savedFruit) { ?>
        <div class="set_list__item slick-generator row-fruit-container-<?php echo $key ?>" data-slick-gen="<?php echo $key ?>">
            <div class="set_list__item-info">
                <div class="set-slider slider-<?php echo $key ?>">

                    <?php foreach ($savedFruit as $product) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($product['id']), 'single-post-thumbnail');
                        ?>
                        <div data-post-id="<?php echo $product['id']; ?>"
                             data-product-price="<?php echo $product['price']; ?>">
                            <img src="<?php echo $image[0]; ?>" alt="">
                            <div class="set_list__item-info--name">
                                <?php echo $product['title']; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div class="slider-nav slider-nav-<?php echo $key ?>">

                    <?php foreach ($savedFruit as $product) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($product['id']), 'single-post-thumbnail');
                        ?>
                        <div data-post-id="<?php echo $product['id']; ?>"
                             data-product-price="<?php echo $product['price']; ?>"
                             data-current-set="<?php echo $key ?>"
                             data-qty="<? echo $product['qty'] ?>"
                             data-step="<? echo $product['step'] ?>"
                             data-max="<? echo $product['max'] ?>">
                            <img src="<?php echo $image[0]; ?>" alt="<? echo $product['title'] ?>">
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="set_list__item-weight">
                <button class="set_list__item-weight--increase qtyBtn" data-set-qty-number="<?php echo $key ?>"
                        onclick="this.parentNode.querySelector('#fruit-price-input-<?php echo $key ?>').stepUp()">+
                </button>
                <input id="fruit-price-input-<?php echo $key ?>" class="set_list__item-weight--input" type="number"
                       data-set-number="<?php echo $key ?>" value="<?php echo $savedFruit[1]['qty'] ?>"
                       step="<?php echo $savedFruit[1]['step'] ?>"
                       min="<?php echo $savedFruit[1]['qty'] ?>"
                       max="<?php echo $savedFruit[1]['max'] ?>">
                <button class="set_list__item-weight--decrease qtyBtn" data-set-qty-number="<?php echo $key ?>"
                        onclick="this.parentNode.querySelector('#fruit-price-input-<?php echo $key ?>').stepDown()">
                    -
                </button>
            </div>
            <div class="set_list__item-price">
                <span id="fruit-item-price-<?php echo $key ?>" class="set_list__item-price--wrap">
                    <?php echo number_format($savedFruit[1]['price'], 2 , '.', ' ') ?></span>
                ₽/ед
            </div>
            <div class="set_list__item-price">
                <span id="fruit-price-<?php echo $key ?>" class="set_list__item-price--wrap"
                      data-base-price="<?php echo $savedFruit[1]['price'] ?>">
                    <?php echo number_format($savedFruit[1]['price'] * $savedFruit[1]['qty'], 2, '.', ' ')?></span>
                ₽
            </div>

            <div class="set_list__item-remove">
                <button class="set_list__item-remove--btn" data-remove-nuber="<?php echo $key ?>"></button>
            </div>
        </div>
        <?php
        $nextRowNumber = (int)$key + 1;
    }
    ?>
</div>
<div class="new_set_front clone">
    <div class="set_list__item">
        <div class="set_list__item-price sum_price">
            Итого: <span class="fruit-sum-price" data-fruit-sum-price=""></span>
            ₽
        </div>
        <button class="set_list__addNew fruit_add_to_cart">Добавить в корзину</button>
    </div>
</div>

