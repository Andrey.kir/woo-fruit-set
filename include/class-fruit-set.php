<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit();
}

if ( ! class_exists( 'FRUIT_Set' ) ) {

    class FRUIT_Set {

        private static $instance;

        public static function get_instance() {
            if ( ! isset( self::$instance ) ) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct() {
            require_once FRUIT_SET_DIR . 'include/class-fruit-set-assets.php';
            require_once FRUIT_SET_DIR . 'include/class-fruit-set-post-type.php';
//            require_once FRUIT_SET_DIR . 'include/class-fruit-set-carbon.php';
            require_once FRUIT_SET_DIR . 'include/class-fruit-set-templates.php';
            require_once FRUIT_SET_DIR . 'include/class-fruit-set-init-custom-field.php';
            require_once FRUIT_SET_DIR . 'include/class-fruit-set-add-to-cart.php';
//            require_once FRUIT_SET_DIR . 'include/class-cpt-films-shortcode.php';
//            require_once FRUIT_SET_DIR . 'include/class-cpt-films-tinymce.php';
            add_action( 'init', [$this, 'checkRules'], 10 );
        }

        public function checkRules(){
            if ( get_option( 'fruit_set_rewrite_rules' ) ) {
                flush_rewrite_rules();
                delete_option( 'fruit_set_rewrite_rules' );
            }
        }

        public static function fruit_set_activate() {
            if ( ! get_option( 'fruit_set_rewrite_rules' ) ) {
                add_option( 'fruit_set_rewrite_rules', true );
            }
        }

        public static function fruit_set_deactivate() {
            flush_rewrite_rules();
        }
    }

    FRUIT_Set::get_instance();
}