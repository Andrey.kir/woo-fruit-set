<?php global $savedFruits ?>
<ul class="set_composition">
    <?php
    foreach ($savedFruits AS $key=>$savedFruit) {?>
        <li class="set-list-<? echo $key ?>">- <span class="set-list-name"><?php echo $savedFruit[1]['title'] ?></span>
            (количество: <span class="set-list-qty"><?php echo $savedFruit[1]['qty']?></span>);</li>
    <?php } ?>
</ul>
<div class="set_add">
    <div class="set_list__item-price sum_price">
        Итого: <span class="fruit-sum-price-top"></span>
        ₽
    </div>
    <button class="set_list__addNew fruit_add_to_cart">Добавить в корзину</button>
</div>