<?php
if (!defined('ABSPATH')) {
    exit();
}

if (!class_exists('FRUIT_Set_Assets')) {
    class FRUIT_Set_Assets
    {

        private static $instance;

        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct()
        {
            add_action('wp_enqueue_scripts', [$this, 'register_frontend_scripts']);
            add_action('admin_enqueue_scripts', [$this, 'register_plugin_backend_scripts']);
        }

        public function register_frontend_scripts()
        {

            wp_register_style('woo-fruit-set-style', plugins_url('woo-fruit-set/include/assets/css/fruit-set.css'));
            wp_register_style('woo-fruit-set-slick', plugins_url('woo-fruit-set/include/assets/css/slick.css'));

            wp_register_script('woo-fruit-set-slick-js', plugins_url('woo-fruit-set/include/assets/js/slick.min.js'), 'jquery');
            wp_register_script('woo-fruit-set-js', plugins_url('woo-fruit-set/include/assets/js/fruit-set.js'), 'jquery');

            wp_enqueue_style('woo-fruit-set-style');
            wp_enqueue_style('woo-fruit-set-slick');
            wp_enqueue_script('woo-fruit-set-slick-js');
            wp_enqueue_script('woo-fruit-set-js');

            wp_localize_script( 'woo-fruit-set-js', 'fruit_ajax',
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
        }

        public function register_plugin_backend_scripts()
        {
            wp_register_style('woo-fruit-set-admin-style', plugins_url('woo-fruit-set/include/assets/css/fruit-admin.css'));
            wp_register_style('woo-fruit-set-style', plugins_url('woo-fruit-set/include/assets/css/fruit-set.css'));
            wp_register_style('woo-fruit-set-slick', plugins_url('woo-fruit-set/include/assets/css/slick.css'));
            wp_register_style('woo-fruit-set-select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css');

            wp_deregister_script( 'jquery' );

            wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1' );
            wp_register_script('woo-fruit-set-slick-admin-js', plugins_url('woo-fruit-set/include/assets/js/slick.min.js'), 'jquery');
            wp_register_script('woo-fruit-set-select2-admin-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js', 'jquery');
            wp_register_script('woo-fruit-set-admin-js', plugins_url('woo-fruit-set/include/assets/js/fruit-admin.js'), 'jquery');



            wp_enqueue_style('woo-fruit-set-admin-style');
            wp_enqueue_style('woo-fruit-set-style');
            wp_enqueue_style('woo-fruit-set-slick');
            wp_enqueue_style('woo-fruit-set-select2');
            wp_enqueue_script('woo-fruit-set-slick-admin-js');
            wp_enqueue_script('woo-fruit-set-select2-admin-js');
            wp_enqueue_script('woo-fruit-set-admin-js');

        }

    }

    FRUIT_Set_Assets::get_instance();
}