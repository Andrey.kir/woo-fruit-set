jQuery(document).ready(function ($) {


    $('#fruit-main-container').on('change', '.fruit_categories', function () {
        let catValue = this.value;
        let currentSet = $(this).parents('.row').attr('data-row-number');
        let currentCol = $(this).parents('.row').attr('data-col-number');

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=fruit_action&input_val=' + catValue + '&currentSet=' + currentSet
                + '&currentCol=' + currentCol,
            success: function (data) {
                $('#fruit-products-category-' + currentSet + '-' + currentCol).html(data);
                $('.add-current-set-' + currentSet + '-' + currentCol).addClass('active');
            }
        });
    });

    $('#fruit-main-container').on('click', '.add-row', function () {
        var element = $(this);
        let nextSet = Number($(this).attr('data-row-number')) + 1;
        let nextCol = 1;

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=add_box&nextSet=' + nextSet + '&nextCol=' + nextCol,
            success: function (data) {
                $(element).parents('.add-div').remove();
                $('#fruit-main-container').append(data);
            }
        });
    });

    $('#fruit-main-container').on('click', '.add-set', function (e) {
        e.preventDefault();
        let currentSet = Number($(this).attr('data-row-number')),
            currentCol = Number($(this).attr('data-col-number')),
            nextCol = currentCol + 1,
            parent = $('#parent_container_' + currentSet),
            colNum = parent.find('.row').length;

        let container = $('#fruit-products-category-' + currentSet + '-' + currentCol),
            select = $('#product-set-' + currentSet + '-' + currentCol),
            currentId = select.find(':selected').val(),
            productTitle = select.find(':selected').text(),
            image = container.find('#product-set-image-' + currentSet + '-' + currentCol)[0].outerHTML,
            input = "<input type='hidden' name='set-item-" + currentSet + "-" + currentCol + "' value='" + currentId + "'>";

        container.html("<div class='set_list__item-info'>" + productTitle + image + input + "</div>");

        $('.add-current-set-' + currentSet + '-' + currentCol).removeClass('active');
        $('.remove-current-set-' + currentSet + '-' + currentCol).addClass('active');

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=add_fruit_column&nextSet=' + currentSet + '&nextCol=' + nextCol,
            success: function (data) {
                if (parent.find('.add-col').length === 0 && colNum < 5) {
                    $('#parent_container_' + currentSet).append(data);
                }
            }
        });
    });

    $('#fruit-main-container').on('click', '.remove-set', function (e) {
        e.preventDefault();
        let currentSet = Number($(this).attr('data-row-number')),
            currentCol = Number($(this).attr('data-col-number')),
            postId = $('#post_ID').val(),
            btn = $(this);

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=remove_fruit_meta&currentSet=' + currentSet + '&postId=' + postId,
            success: function (data) {
                // $('#parent_container_' + currentSet).remove();
                btn.parents('.row').remove();
            }
        });
    });

    $('#fruit-main-container').on('click', '.add-col', function (e) {
        e.preventDefault();

        let currentSet = Number($(this).attr('data-row-number'));
        let currentCol = Number($(this).attr('data-col-number'));
        let container = $(this).parent();

        container.find('.add-col').remove();

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=fruit_column_content&nextSet=' + currentSet + '&nextCol=' + currentCol + '&dieFun=true',
            success: function (data) {
                container.append(data);
            }
        });
    })

});