jQuery(document).ready(function ($) {

    $('.set_list').on('change keyup', '.set_list__item-weight--input', function () {
        let catValue = $(this).val();
        let currentSet = $(this).attr('data-set-number');
        let basePrice = $('#fruit-price-' + currentSet).attr('data-base-price');
        let sumPrice = catValue * basePrice;

        $('#fruit-price-' + currentSet).html(sumPrice.toFixed(2));
        $('.set_composition').find('.set-list-' + currentSet + ' .set-list-qty').text(catValue);

        summaryPrice();

    });

    $('.qtyBtn').on('click', function () {
        let currentSet = $(this).attr('data-set-qty-number');
        let qty = $('#fruit-price-input-' + currentSet).val();
        let basePrice = $('#fruit-price-' + currentSet).attr('data-base-price');
        let sumPrice = qty * basePrice;

        $('#fruit-price-' + currentSet).html(sumPrice.toFixed(2));
        $('.set_composition').find('.set-list-' + currentSet + ' .set-list-qty').text(qty);

        summaryPrice();

    });

    $('.set_list').on('click change', '.slider-nav .slick-current', function () {

        let currentSet = $(this).attr('data-current-set'),
            basePrice = $(this).attr('data-product-price'),
            // qty = $('#fruit-price-input-' + currentSet).val(),
            qty = $(this).attr('data-qty'),
            step = $(this).attr('data-step'),
            max = $(this).attr('data-max'),
            title = $(this).find('img').attr('alt');

        let sumPrice = basePrice * qty;

        $('.set_composition').find('.set-list-' + currentSet + ' .set-list-name').text(title);
        $('.set_composition').find('.set-list-' + currentSet + ' .set-list-qty').text(qty);

        $('#fruit-price-input-' + currentSet).attr({
            value: qty,
            min: qty,
            step: step,
            max: max
        });
        $('#fruit-price-' + currentSet).attr('data-base-price', basePrice);
        $('#fruit-price-' + currentSet).html(sumPrice.toFixed(2));
        $('#fruit-item-price-' + currentSet).html(sumPrice.toFixed(2));

        summaryPrice();
    });

    $('.set_list').on('click', '.add_next_row', function (e) {
        e.preventDefault();
        let nextSet = Number($(this).attr('data-row-number'));

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: 'action=add_front_box&nextSet=' + nextSet,
            success: function (data) {
                $(this).remove();
                $('#fruit-main-container').append(data);
            }
        });
    });

    $('.set_list').on('click', '.set_list__item-remove--btn', function () {
        let currentSet = $(this).attr('data-remove-nuber');
        $('.row-fruit-container-' + currentSet).remove();
    });

    function summaryPrice() {

        let sum = priceCalculating();

        $('.fruit-sum-price').html(sum.toFixed(2));

        $('.fruit-sum-price').attr('data-fruit-sum-price', sum.toFixed(2));
    }

    $('.fruit_add_to_cart').on('click', function (e) {
        e.preventDefault();

        let title = $('.fruit-product_title').text();
        let price = $('.fruit-sum-price').attr('data-fruit-sum-price');
        let description = $('.set_composition').html();
        let set_img_id = $('.set_img').attr('data-thumb-id');

        $.ajax({
            type: 'POST',
            url: fruit_ajax.ajax_url,
            data: 'action=fruit_add_to_cart' +
                '&title=' + title +
                '&price=' + price +
                '&description=' + description +
                '&img_id=' + set_img_id,
            success: function (data) {
                window.location.href = '/cart';
            }
        });
    });

    summaryPrice();
    $('.fruit-sum-price-top').html(priceCalculating().toFixed(2));

    function priceCalculating() {
        let sum = 0;

        $('.set_list__item-weight--input').each(function () {
            let setNumber = $(this).attr('data-set-number');
            let setQTY = $(this).val();
            let setPrice = $('#fruit-price-' + setNumber).attr('data-base-price');

            sum += setQTY * setPrice;
        });
        return sum;
    }

    $('.slick-generator').each(function () {
        let itemNo = $(this).attr('data-slick-gen');

        $('.slider-' + itemNo).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav-' + itemNo
        });
        $('.slider-nav-' + itemNo).slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-' + itemNo,
            dots: false,
            centerMode: true,
            focusOnSelect: true,
            centerPadding: 0,
        });

    });

});