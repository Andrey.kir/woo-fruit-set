<?php
if (!defined('ABSPATH')) {
    exit();
}

if (!class_exists('FRUIT_Set_Init_Custom_Field')) {
    class FRUIT_Set_Init_Custom_Field
    {

        private static $instance;
        public $product_categories;

        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct()
        {
            add_action('add_meta_boxes', [$this, 'fruit_meta_box']);
            add_action('save_post', [$this, 'fruit_save']);

            $this->product_categories = $this->get_fruit_product_categories();


        }

        public function fruit_meta_box()
        {
            $screens = array('set');
            add_meta_box('meta-set', 'Готовый набор', [$this, 'fruit_meta_box_callback'], $screens);
        }

        public function fruit_meta_box_callback($post, $meta)
        {
            $hasFruitMetaHTML = $this->get_saved_post_meta($post);

            echo $hasFruitMetaHTML;
            die();
        }

        public function fruit_save($post_id)
        {
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

            if ($parent_id = wp_is_post_revision($post_id)) {
                $post_id = $parent_id;
            }

            foreach (FRUIT_FIELDS as $field) {
                $rowOfProducts = FRUIT_ITEM_ID_META . $field;
                $itemArr = [];

                foreach (FRUIT_COLUMNS as $col) {
                    $itemIdField = FRUIT_ITEM_ID_META . $field . '-' . $col;

                    $categoryIdField = FRUIT_CATEGORY_ID_META . $field . '-' . $col;
                    if (array_key_exists($itemIdField, $_POST) && array_key_exists($categoryIdField, $_POST)) {
                        if (!is_null($_POST[$itemIdField]) && !is_null($_POST[$categoryIdField])) {

                            $itemArr[$_POST[$itemIdField]] = $_POST[$categoryIdField];

                        }
                    }

                }

                if (count($itemArr) > 0) {

                    $oldMeta = get_post_meta($post_id, $rowOfProducts, true);

                    if (empty($oldMeta)) {
                        add_post_meta($post_id, $rowOfProducts, base64_encode(serialize($itemArr)));
                    } else {
                        update_post_meta($post_id, $rowOfProducts, base64_encode(serialize($itemArr)), $oldMeta);
                    }

                }
            }

        }

        public function get_saved_post_meta($post)
        {

            $nextField = 1;
            $html = '';
            ob_start();
            echo "<div id='fruit-main-container' class='container'>";
//            echo '<pre>';
//            var_dump($rowOfProducts);
            if (is_array(FRUIT_FIELDS)) {
                foreach (FRUIT_FIELDS as $field) {
                    $rowOfProducts = FRUIT_ITEM_ID_META . $field;

                    $itemID = esc_attr(get_post_meta($post->ID, $rowOfProducts, true));

                    if (!empty($itemID)) {

                        $itemID = unserialize(base64_decode($itemID));
                        $nextCol = 1;



                        echo "<div id='parent_container_$field' class='container'>";
                        echo "<h3 style='display:block;width: 100%;margin-top: 0;'>Состав</h3>";
                        foreach ($itemID as $key => $value) {

                            $categoryIdField = FRUIT_CATEGORY_ID_META . $field . '-' . $nextCol;

                            echo "<div  class='row admin-set' data-row-number='$field' data-col-number='$nextCol'>";
                            echo "<div><div>";
                            echo "<select class='fruit_categories' name='$categoryIdField'>";
                            foreach ($this->product_categories as $category) {
                                if ($value == $category->term_id) {
                                    echo "<option value='" . $category->term_id . "' selected>" . $category->name . "</option>";
                                } else {
                                    echo "<option value='" . $category->term_id . "'>" . $category->name . "</option>";
                                }
                            }
                            echo "</select>";
                            echo "</div>";
                            echo "<div>";
                            echo "<div id='fruit-products-category-$field-$nextCol'>";
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($key), 'single-post-thumbnail'); ?>
                            <div class="set_list__item-info">
                                <?php
                                echo get_the_title($key);
                                ?>
                                <div data-post-id="<?php echo $itemID; ?>">
                                    <img id="product-set-image-<? echo $field ?>-<? echo $nextCol ?>"
                                         src="<?php echo $image[0]; ?>" alt="">
                                </div>
                                <input type='hidden' name='set-item-<?php echo $field ?>-<? echo $nextCol ?>'
                                       value='<?php echo $key ?>'>
                            </div>
                            <?php
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='btns-container'>";
                            echo "<div id='fruit-choise-button-$field-$nextCol'>";
                            echo "<button class='choise-button add-set add-current-set-$field-$nextCol' data-row-number='$field' data-col-number='$nextCol'>Сохранить</button>";
                            echo "<button class='choise-button remove-set remove-current-set-$field-$nextCol active' data-row-number='$field' data-col-number='$nextCol'>Удалить</button>";
                            echo "</div></div>";
                            echo "</div>";
                            echo "</div>";

                            $nextCol++;
                        }

                        if ($nextCol <= 5) {
                            echo "<div class='row admin-set' data-row-number='$field' data-col-number='$nextCol'>";
                            echo "<div class='add-col' data-row-number='$field' data-col-number='$nextCol'>+</div>";
                            echo "</div>";
                        }

                        echo "</div>";
                        $nextField = $field + 1;
                    }
                }
            }
            echo "<div class='add-div'>";
            echo "<span class='add-row' data-row-number='$nextField'>Добавить продукт <strong>+</strong></span>";
            echo "</div>";
            echo "</div>";
            $html .= ob_get_contents();
            ob_clean();
            echo $html;
        }

        public function get_fruit_product_categories()
        {
            $orderby = 'name';

            $order = 'asc';
            $hide_empty = false;
            $cat_args = array(
                'orderby' => $orderby,
                'order' => $order,
                'hide_empty' => $hide_empty,
            );

            return get_terms('product_cat', $cat_args);
        }
    }
}