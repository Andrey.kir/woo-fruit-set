<?php
if (!defined('ABSPATH')) {
    exit();
}

if (!class_exists('FRUIT_Set_Templates')) {

    class FRUIT_Set_Templates
    {

        private static $instance;

        public $savedFruits;
        public $savedFruit;

        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct()
        {
            add_filter('single_template', [$this, 'fruit_single_template']);
            add_action('wp_ajax_add_front_box', [$this, 'add_front_box_callback']);
            add_action('wp_ajax_nopriv_add_front_box', [$this, 'add_front_box_callback']);
            add_action('woocommerce_after_cart_item_name', [$this, 'change_cart_item_name'], 10, 2);
        }

        public function fruit_single_template($single)
        {
            global $post, $savedFruits;

            $this->get_fruit_meta_array($post);

            $savedFruits = $this->savedFruits;
            if ($post->post_type == 'set') {
                if (file_exists(FRUIT_SET_DIR . 'templates/front/fruit-set-single.php')) {
                    return FRUIT_SET_DIR . 'templates/front/fruit-set-single.php';
                }
            }

            return $single;
        }

        public function get_fruit_meta_array($post)
        {

            foreach (FRUIT_FIELDS AS $field) {
                $rowOfProducts = FRUIT_ITEM_ID_META . $field;

                $categoryIdField = FRUIT_CATEGORY_ID_META . $field;

                $itemID = esc_attr(get_post_meta($post->ID, $rowOfProducts, true));

//                    $termID = esc_attr(get_post_meta($post->ID, $categoryIdField, true));
//                $productsList = $this->get_fruit_products_by_term_id($termID);

                if (!empty($itemID)) {
                    $itemID = unserialize(base64_decode($itemID));
                    $col = 1;
                    foreach ($itemID as $key => $value) {
                        $product = wc_get_product($key);

//                        $product = get_product( $key );
                        // connection with quantity plugin
                        $rule = wcqu_get_applied_rule($product);
                        $min = wcqu_get_value_from_rule('min', $product, $rule);
                        $step = wcqu_get_value_from_rule('step', $product, $rule);
                        $max = wcqu_get_value_from_rule('max', $product, $rule);
                        // end connection with quantity plugin


                        $this->savedFruits[$field][$col]['id'] = $key;
                        $this->savedFruits[$field][$col]['term_id'] = $key;
                        $this->savedFruits[$field][$col]['title'] = get_the_title($key);
                        $this->savedFruits[$field][$col]['image'] = wp_get_attachment_image_src(get_post_thumbnail_id($key), 'single-post-thumbnail');

                        // connection with quantity plugin
                        $this->savedFruits[$field][$col]['qty'] = empty($min) ? '1' : $min;
                        $this->savedFruits[$field][$col]['step'] = empty($step) ? '1' : $step;
                        $this->savedFruits[$field][$col]['max'] = empty($max) ? '10' : $max;
                        // end connection with quantity plugin
                        $this->savedFruits[$field][$col]['price'] = $product->get_price();
//                    $this->savedFruits[$field]['products'] = $productsList;
                        $col++;
                    }
                }

            }

        }

        public function get_fruit_products_by_term_id($termID)
        {
            $args = array(
                'post_type' => 'product',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'terms' => $termID,
                        'operator' => 'IN',
                    )
                )
            );
            $productsList = new WP_Query($args);

            return $productsList->posts;
        }

        public function add_front_box_callback()
        {
            include(FRUIT_SET_DIR . 'templates/front/template-parts/lists/set_list_item.php');
        }


        public function change_cart_item_name($cart_item, $cart_item_key)
        {

            $product_descr = wc_get_product($cart_item['product_id'])->description;

            echo '<ul style="font-size: .75rem;">' . $product_descr . '</ul>';
//            do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

        }

    }

    FRUIT_Set_Templates::get_instance();
}
