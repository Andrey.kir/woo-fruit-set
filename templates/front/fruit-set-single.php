<?php
/**
 * Fruit single template
 *
 */

get_header();

?>

    <div id="content" class="blog-wrapper blog-single page-wrapper">
        <!--        -->
        <div class="row row-large <?php if (flatsome_option('blog_layout_divider')) echo 'row-divided '; ?>">

            <div class="large-9 col">
                <?php
                    include(FRUIT_SET_DIR . 'templates/front/template-parts/fruit-single.php');
                ?>
            </div> <!-- .large-9 -->

            <div class="post-sidebar large-3 col">
                <?php
                if (is_active_sidebar('product-sidebar')) {
                    dynamic_sidebar('product-sidebar');
                }
                ?>

            </div><!-- .post-sidebar -->

        </div><!-- .row -->
    </div><!-- #content .page-wrapper -->

<?php get_footer();
