<?php
/*
Plugin Name: Woocommerce Fruit Set
Version: 1.0.2
Description: Создание набора продуктов, возможность добавления вариаций продуктов, добавление набора в корзину одним товаром
Author: Victoria Dmitrenko
Author URI: https://www.linkedin.com/in/space-dementias/
*/

if (!defined('ABSPATH')) {
    exit();
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

define('FRUIT_SET_FILE', __FILE__);
define('FRUIT_SET_BASE', plugin_basename(FRUIT_SET_FILE));
define('FRUIT_SET_DIR', plugin_dir_path(FRUIT_SET_FILE));
define('FRUIT_SET_URI', plugins_url('/', FRUIT_SET_FILE));
define('FRUIT_ITEM_ID_META', 'set-item-');
define('FRUIT_CATEGORY_ID_META', 'category-' . FRUIT_ITEM_ID_META);

const FRUIT_FIELDS = array('0' => '1', '1' => '2', '2' => '3', '3' => '4', '4' => '5', '5' => '6', '6' => '7', '7' => '8', '8' => '9', '9' => '10');
const FRUIT_COLUMNS = array('0' => '1', '1' => '2', '2' => '3', '3' => '4', '4' => '5');



require_once FRUIT_SET_DIR . 'include/class-fruit-set.php';

register_activation_hook(__FILE__, 'FRUIT_Set::fruit_set_activate');
register_deactivation_hook(__FILE__, 'FRUIT_Set::fruit_set_deactivate');


if (is_admin()) {
    add_action('load-post.php', 'init_customField');
    add_action('load-post-new.php', 'init_customField');
}

function init_customField()
{
    new FRUIT_Set_Init_Custom_Field();
}

function fruit_ajax_function()
{

    $args = array(
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'terms' => $_POST['input_val'],
                'operator' => 'IN',
            )
        ),
        'posts_per_page' => -1
    );
    $products = new WP_Query($args);

    ob_start();
    ?>
    <div class="set_list__item-info">
        <p>Выберите продукт:</p>
        <select id="product-set-<?php echo $_POST['currentSet'] ?>-<?php echo $_POST['currentCol'] ?>" name="state">
            <?php
            foreach ($products->posts AS $product) {
                ?>
                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'single-post-thumbnail'); ?>
                <option value="<?php echo $product->ID; ?>" data-image="<? echo $image[0] ?>">
                    <?php echo $product->post_title; ?>
                </option>
                <?php
            }
            ?>
        </select>

        <img id="product-set-image-<? echo $_POST['currentSet'] ?>-<?php echo $_POST['currentCol'] ?>" src="">

    </div>

    <script>
        jQuery(document).ready(function ($) {
            $("#product-set-<?php echo $_POST['currentSet']?>-<?php echo $_POST['currentCol']?>").select2({
                templateSelection: formatState
            });

            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }


                $("#product-set-image-<?php echo $_POST['currentSet']?>-<?php echo $_POST['currentCol']?>")
                    .attr('src', state.element.dataset.image);

                var $state = $(
                    '<span>' + state.text + '</span>'
                );
                return $state;
            }
        });
    </script>
    <?php

    $html = ob_get_contents();
    ob_clean();

    echo $html;
    die();
}

add_action('wp_ajax_fruit_action', 'fruit_ajax_function');
add_action('wp_ajax_nopriv_fruit_action', 'fruit_ajax_function');

function add_meta_box_callback()
{
    echo "<div id='parent_container_" . $_POST['nextSet'] . "' class='container'>";
    echo "<h3 style='display:block;width: 100%;margin-top: 0;'>Состав</h3>";
    echo "<div class='row admin-set' data-row-number='" . $_POST['nextSet'] . "' data-col-number='" . $_POST['nextCol'] . "'>";

    fruit_column();

    echo "</div>";
    echo "</div>";
    echo "<div class='add-div'>";
    echo "<span class='add-row' data-row-number='" . $_POST['nextSet'] . "'  data-col-number='" . $_POST['nextCol'] . "'>Добавить продукт <strong>+</strong></span>";
    echo "</div>";
    die();
}

add_action('wp_ajax_add_box', 'add_meta_box_callback');
add_action('wp_ajax_nopriv_add_box', 'add_meta_box_callback');

function remove_fruit_meta_callback()
{

//    if (!empty($_POST['currentSet']) && !empty($_POST['postId'])) {
//        $itemIdfield     =  FRUIT_ITEM_ID_META.$_POST['currentSet'];
//        $categoryIdField =  FRUIT_CATEGORY_ID_META.$_POST['currentSet'];
//
//        delete_post_meta( $_POST['postId'], $itemIdfield );
//        delete_post_meta( $_POST['postId'], $categoryIdField);
//    }

    die();
}

add_action('wp_ajax_remove_fruit_meta', 'remove_fruit_meta_callback');
add_action('wp_ajax_nopriv_remove_fruit_meta', 'remove_fruit_meta_box_callback');

function add_fruit_column_callback()
{
    echo "<div class='row admin-set' data-row-number='" . $_POST['nextSet'] . "' data-col-number='" . $_POST['nextCol'] . "'>";
    echo "<div class='add-col' data-row-number='" . $_POST['nextSet'] . "' data-col-number='" . $_POST['nextCol'] . "'>+</div>";
    echo "</div>";

    die();
}

add_action('wp_ajax_add_fruit_column', 'add_fruit_column_callback');
add_action('wp_ajax_nopriv_add_fruit_column', 'add_fruit_column_callback');


function fruit_column()
{
    $orderby = 'name';
    $order = 'asc';
    $hide_empty = false;
    $cat_args = array(
        'orderby' => $orderby,
        'order' => $order,
        'hide_empty' => $hide_empty,
    );

    $product_categories = get_terms('product_cat', $cat_args);
    echo "<div>";
    echo "<div>";
    echo "<select class='fruit_categories' name='category-set-item-" . $_POST['nextSet'] . "-" . $_POST['nextCol'] . "'>";
    foreach ($product_categories AS $category) {
        echo "<option value='" . $category->term_id . "'>" . $category->name . "</option>";
    }
    echo "</select>";
    echo "</div>";
    echo "<div>";
    echo "<div id='fruit-products-category-" . $_POST['nextSet'] . "-" . $_POST['nextCol'] . "'>";

    echo "</div>";
    echo "</div>";
    echo "<div class='btns-container'>";
    echo "<div id='fruit-choise-button-" . $_POST['nextSet'] . "-" . $_POST['nextCol'] . "'>";
    echo "<button class='choise-button add-set add-current-set-" . $_POST['nextSet'] . "-" . $_POST['nextCol'] . "' data-row-number='" . $_POST['nextSet'] . "' data-col-number='" . $_POST['nextCol'] . "'>Сохранить</button>";
    echo "<button class='choise-button remove-set remove-current-set-" . $_POST['nextSet'] . "-" . $_POST['nextCol'] . "' data-row-number='" . $_POST['nextSet'] . "' data-col-number='" . $_POST['nextCol'] . "'>Удалить</button>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

    if ($_POST['dieFun']) {
        die();
    }
}

add_action('wp_ajax_fruit_column_content', 'fruit_column');
add_action('wp_ajax_nopriv_fruit_column_content', 'fruit_column');