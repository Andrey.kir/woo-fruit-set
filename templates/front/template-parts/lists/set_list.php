<?php
global $savedFruits;
//echo "<pre>";
//var_dump($savedFruits);
//echo "</pre>";
?>
<div class="set_list">
    <?php foreach ($savedFruits AS $key => $savedFruit) { ?>
        <div class="set_list__item row-fruit-container-<?php echo $key ?>">
            <div class="set_list__item-info slider-<?php echo $key ?>">
                <div data-post-id="<?php echo $savedFruit['id']; ?>"
                     data-product-price="<?php echo $savedFruit['price'] ?>">
                    <img src="<?php echo $savedFruit['image']['0'] ?>"
                         alt="">
                    <div class="set_list__item-info--name">
                        <?php echo $savedFruit['title'] ?>
                    </div>
                </div>
                <?php
                foreach ($savedFruit['products'] AS $product) {
                    $product = wc_get_product($product->ID);
                    ?>
                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()), 'single-post-thumbnail'); ?>
                    <div data-post-id="<?php echo $product->get_id(); ?>"
                         data-product-price="<?php echo $product->get_price(); ?>">
                        <img src="<?php echo $image[0]; ?>" alt="">
                        <div class="set_list__item-info--name">
                            <p>
                                <?php echo $product->get_title(); ?>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="set_list__item-weight">
                <button class="set_list__item-weight--increase qtyBtn" data-set-qty-number="<?php echo $key ?>"
                        onclick="this.parentNode.querySelector('#fruit-price-input-<?php echo $key ?>').stepUp()">+
                </button>
                <input id="fruit-price-input-<?php echo $key ?>" class="set_list__item-weight--input" type="number"
                       data-set-number="<?php echo $key ?>" value="<?php echo $savedFruit['qty'] ?>" step="1" min="1"
                       max="10">
                <button class="set_list__item-weight--decrease qtyBtn" data-set-qty-number="<?php echo $key ?>"
                        onclick="this.parentNode.querySelector('#fruit-price-input-<?php echo $key ?>').stepDown()">-
                </button>
            </div>
            <div class="set_list__item-price">
                <span id="fruit-price-<?php echo $key ?>" class="set_list__item-price--wrap"
                      data-base-price="<?php echo $savedFruit['price'] ?>"><?php echo $savedFruit['price'] ?>.00</span>
                ₽
            </div>
            <div class="set_list__item-remove">
                <button class="set_list__item-remove--btn" data-remove-nuber="<?php echo $key ?>"></button>
            </div>
        </div>
        <script>
            jQuery('.slider-<?php echo $key ?>').slick({
                draggable: false,
                swipe: false,
                vertical: true,
                centerMode: true,
                centerPadding: '0px',
                appendArrows: jQuery('.slider-<?php echo $key ?>'),
                prevArrow: '<button type="button" class="slide-btn-get btn btn-prev" data-slide-id="<?php echo $key ?>">+</button>',
                nextArrow: '<button type="button" class="slide-btn-get btn btn-next" data-slide-id="<?php echo $key ?>">-</button>'
            });
        </script>
        <?php
        $nextRowNumber = (int)$key + 1;
    }
    ?>

</div>
<div class="new_set_front clone">
    <div class="set_list__item">
        <div class="top_front_select">
            Выберите категорию товаров:
            <select class="next-select-value-<?php echo $nextRowNumber ?>">
                <?php
                foreach ($savedFruits['categories'] AS $category) {
                    echo "<option value='" . $category->term_id . "'>" . $category->name . "</option>";
                }
                ?>
            </select>
            <button class="add_next_row set_list__addNew" data-row-number="<?php echo $nextRowNumber ?>">
                Добавить к набору
            </button>
        </div>
    </div>
    <div class="set_list__item">
        <div class="set_list__item-price sum_price">
            Итого: <span class="fruit-sum-price" data-fruit-sum-price=""></span>
            ₽
        </div>
        <button class="set_list__addNew fruit_add_to_cart">Добавить в корзину</button>
    </div>
</div>