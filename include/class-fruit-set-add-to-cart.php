<?php
if (!defined('ABSPATH')) {
    exit();
}

if (!class_exists('FRUIT_add_to_cart')) {
    class FRUIT_add_to_cart
    {
        private static $instance;

        public static function get_instance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public function __construct()
        {
            add_action('wp_ajax_fruit_add_to_cart', [$this, 'fruit_add_to_cart_function']);
            add_action('wp_ajax_nopriv_fruit_add_to_cart', [$this, 'fruit_add_to_cart_function']);
            add_action('woocommerce_add_to_cart', [$this, 'custome_add_to_cart']);
        }

        public function fruit_add_to_cart_function()
        {
            ini_set('error_reporting', E_ALL);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);

            $post_id = wp_insert_post(array(
                    'post_title' => $_POST['title'],
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'post_content' => $_POST['description'],
                    'post_excerpt' => $_POST['description']
            ));

            $meta = get_post_meta($post_id, 'expire_date', true);

            if( $meta == '' ) {
                $timestamp_1day = strtotime("+1 day");
                update_post_meta( $post_id, 'expire_date', $timestamp_1day);
            }

            wp_set_object_terms( $post_id, 'simple', 'product_type' );
            update_post_meta( $post_id, '_visibility', 'hidden' );
            update_post_meta( $post_id, '_stock_status', 'instock');
            update_post_meta( $post_id, 'total_sales', '0' );
            update_post_meta( $post_id, '_downloadable', 'no' );
            update_post_meta( $post_id, '_virtual', 'no' );
            update_post_meta( $post_id, '_regular_price', $_POST['price'] );
            update_post_meta( $post_id, '_sale_price', $_POST['price'] );
            update_post_meta( $post_id, '_purchase_note', '' );
            update_post_meta( $post_id, '_featured', $_POST['img_id'] );
            update_post_meta( $post_id, '_sku', $_POST['title'] );
            update_post_meta( $post_id, '_product_attributes', array() );
            update_post_meta( $post_id, '_sale_price_dates_from', '' );
            update_post_meta( $post_id, '_sale_price_dates_to', '' );
            update_post_meta( $post_id, '_price', $_POST['price'] );
            update_post_meta( $post_id, '_sold_individually', '' );
            update_post_meta( $post_id, '_manage_stock', 'yes' );
            update_post_meta( $post_id, '_shipping', 'yes' );
            wc_update_product_stock($post_id, '10', 'set');
            update_post_meta( $post_id, '_backorders', 'no' );

            set_post_thumbnail( $post_id, $_POST['img_id'] );

            $this->custome_add_to_cart($post_id);

            die();
        }


        public function custome_add_to_cart($post_id) {
            global $woocommerce;

            $product_id = $post_id;

            $found = false;

            if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
                foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
                    $_product = $values['data'];
                    if ( $_product->id == $product_id )
                        $found = true;
                }
                if ( ! $found )
                    WC()->cart->add_to_cart( $product_id );
            } else {
                WC()->cart->add_to_cart( $product_id );
                var_dump(WC()->cart->add_to_cart( 1999 ));
            }
        }
    }
    FRUIT_add_to_cart::get_instance();
}